import express from 'express';
import mysql from 'mysql';
import cors from 'cors';
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import { body, validationResult } from 'express-validator';


export const app = express();
dotenv.config();
app.use(cors());
app.use(bodyParser.json());

type Token = {
	email: String,
	iat: Date,
	exp: Date
}

const db = mysql.createConnection({
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_NAME
});

function generateAccessToken(email: String) {
	return jwt.sign({ 'email': email }, process.env.TOKEN_SECRET, { expiresIn: '1800s' });
}


app.post(
	'/api/login', 
	body('email').isEmail(),
	body('password').isString(),
	(req: express.Request, res: express.Response) => {

		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		let sql = "SELECT * FROM users WHERE email = ? AND  password = ?;"
		let values = [req.body.email, req.body.password];

		db.query(sql, values, function(err, data) {
			if (err) return res.status(400);

			if (data.length > 0) {	
				const token = generateAccessToken(req.body.email);
				return res.status(200).json(token);
			} else {
				return res.status(401);
			}
		});
	}
);

app.post(
	'/api/signup', 
	body('firstname').isString(),
	body('lastname').isString(),
	body('email').isEmail(),
	body('password').isString(),
	(req: express.Request, res: express.Response) => {

		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		let sql = "INSERT INTO users (first_name, last_name, email, password) VALUES ?";
		let values = [[req.body.firstname, req.body.lastname, req.body.email, req.body.password]];

		db.query(sql, [values], function(err, data) {
			if (err) return res.status(409);

			const token = generateAccessToken(req.body.email);
			return res.status(200).json(token);
		});
	}
);


app.get(
	'/api/name', 
	(req: express.Request, res: express.Response) => {

		const authHeader = req.headers['authorization'];
		const token = authHeader && authHeader.split(' ')[1];
  
		if (token == null) return res.status(401);
	
		jwt.verify(token, process.env.TOKEN_SECRET, (err, jwtInfo: Token) => {
			if (err) return res.status(401);

			let sql = "SELECT * FROM users WHERE email = ?;"
			let values = [jwtInfo.email];

			db.query(sql, values, function(err, data) {
				if (err) return res.status(400);
		
				if (data.length > 0) {	
					return res.status(200).json(data[0].first_name + ' ' + data[0].last_name);
				} else {
					return res.status(403);
				}
			});
		});
	}
);

app.get(
	'/api/verify', 
	(req: express.Request, res: express.Response) => {

		const authHeader = req.headers['authorization'];
		const token = authHeader && authHeader.split(' ')[1];
	
		if (token == null) return res.status(401);
		
		jwt.verify(token, process.env.TOKEN_SECRET, (err, data: Token) => {
			if (err) return res.status(403);
			res.status(200).json('success');
		});
	}
);


app.listen( 8080, () => console.log(`API is running`));
