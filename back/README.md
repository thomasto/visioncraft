# Back-end

### Installation

Installing modules `npm install`

Starting the app `npm start`

Application URL: [http://localhost:8000](http://localhost:8000)


Migration/SQL command for the user table is in `migrations.sql` file. 

Database credentials should be inserted in the `.env` file.

### Packages used

Express for framework. Express-validator for sanitizing and validating API arguments.

Tokens are done and validated by jsonwebtoken package.

### Overview

There are 4 API routes:

Logging in - jsonwebtoken is returned on success.

Sign up - jsonwebtoken is returned on success.

Verify - for checking if the token is valid and not expired

Name - for getting the name of the user, this route checks the validity of jwt.



# Front-end

### Installation

Installing modules `npm install`

Starting the app `npm start`

Running the tests `npm test`


Application URL: [http://localhost:3000](http://localhost:3000)

Backend API base url can be changed in `index.tsx` under `axios.defaults.baseURL`

### Packages used

Material UI react package is used for the styling and basic front-end components. API calls are done with axios.
Testing is done with the default [testing-library](https://testing-library.com/). Mock API is done with `msw` / Mock Service Worker package.

### Overview

User can sign up or log in to the application. After the successful login or sign up user sees the home page with his/her name. User stays logged in when the page is refreshed. User can not sign up with an email which is already used. Authentication is built on JSON web token. 

