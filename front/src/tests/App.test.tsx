// __tests__/fetch.test.js
import React from 'react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import App from '../App';
import userEvent from '@testing-library/user-event';
import { MemoryRouter } from 'react-router-dom';

const server = setupServer(
	rest.post('/api/login', (req, res, ctx) => {
		if ( req.body ){
			if ( JSON.stringify(req.body) !== JSON.stringify({email: "test@email.test", password: "password"}) ){
				return res(ctx.status(401));
			} else {
				return res(ctx.json('test-jwt-token'));
			}
		} else {
			return res(ctx.status(401));
		}

  	}),

	rest.post('/api/signup', (req, res, ctx) => {
		if ( req.body ){
			if ( JSON.stringify(req.body) !== JSON.stringify({email: "test@email.test", password: "password", firstname: "user", lastname: "test"}) ){
				return res(
					ctx.status(401)
				)
			} else {
				return res(ctx.json('test-jwt-token'));
			}
		} else {
			return res(ctx.status(401));
		}

  	}),

	rest.get('/api/name', (req, res, ctx) => {
		if ( req.headers.get('Authorization') !== 'Bearer test-jwt-token' ){
			return res(
				ctx.status(401)
			)
		} 

		return res(ctx.json('test user'));
  	}),

	rest.get('/api/verify', (req, res, ctx) => {
		if ( req.headers.get('Authorization') !== 'Bearer test-jwt-token' ){
			return res(
				ctx.status(401)
			)
		} 
		return res(ctx.json('success'));
  	})
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())


test('failed login', async () => {
	render(<App /> , { wrapper: MemoryRouter });
	await userEvent.type(screen.getByLabelText(/Email/i), 'vale@email.test')
	await userEvent.type(screen.getByLabelText(/Password/i), 'password')

	userEvent.click(screen.getByRole('button', { name: /send/i }))

	expect(await screen.findByText('Wrong username or password!')).toBeInTheDocument()
})


test('successful login', async () => {
	render(<App /> , { wrapper: MemoryRouter });
	await userEvent.type(screen.getByLabelText(/Email/i), 'test@email.test')
	await userEvent.type(screen.getByLabelText(/Password/i), 'password')

	userEvent.click(screen.getByRole('button', { name: /send/i }))

	expect(await screen.findByText('You are logged in as test user!')).toBeInTheDocument()
})


test('successful registration', async () => {
	render(<App /> , { wrapper: MemoryRouter });
	userEvent.click(screen.getByRole('button', { name: /signup/i }))

	await userEvent.type(screen.getByLabelText(/Email/i), 'test@email.test')
	await userEvent.type(screen.getByLabelText(/Password/i), 'password')
	await userEvent.type(screen.getByLabelText(/First name/i), 'test')
	await userEvent.type(screen.getByLabelText(/Last name/i), 'user')

	userEvent.click(screen.getByRole('button', { name: /send/i }))


	expect(await screen.findByText('You are logged in as test user!')).toBeInTheDocument()
})