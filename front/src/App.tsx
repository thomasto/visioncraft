import { useState, useEffect } from 'react';
import axios from 'axios';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
	Redirect
} from "react-router-dom";
import { 
	Container,
	AppBar,
	Toolbar,
	Button
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Login from "./components/Login";
import Home from "./components/Home";
import Signup from "./components/Signup";


/* styling */
const useStyles = makeStyles((theme) => ({
	button: {
		textDecoration: 'none',
		color: 'white'
	},
	main: {
		marginTop: theme.spacing(6)
	}
}));


function App() {
	const [isAuth, setIsAuth] = useState(false);
	const [loginMessage, setLoginMessage] = useState('');
	const [signupMessage, setSignupMessage] = useState('');
	const classes = useStyles(); 

	/* helper function, called from login and sign up functions */
	const authHelper = (token: string) => {
		localStorage.setItem('jwtToken', token);
		axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
		setIsAuth(true);
	}

	const login = (email: string, password: string): void => {
		axios.post(`/api/login`, { email, password })
			.then(res => {
				authHelper(res.data);
			})
			.catch((err) => {
				if (err.response.status === 401){
					setLoginMessage('Wrong username or password!');
				} else {
					setLoginMessage('Something went wrong!');
				}
			}
		)
	}

	const register = (email: string, password: string, firstname: string, lastname: string): void => {
		axios.post(`/api/signup`, { email, password, firstname, lastname })
			.then(res => {
				authHelper(res.data);
			})
			.catch((err) => {
				if (err.response.status === 409){
					setSignupMessage('User with this email already exists!');
				} else {
					setSignupMessage('Something went wrong!');
				}
			}
		)
	}

	const logout = (): void => {
		localStorage.removeItem('jwtToken');
		axios.defaults.headers.common['Authorization'] = '';
		setIsAuth(false);
		setLoginMessage('');
		setSignupMessage('');
	}

	
	useEffect(() => {
		/*
			check if user is already logged in,
			after that, check if the login token is valid and not expired
		*/
		if ( localStorage.getItem('jwtToken') ) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('jwtToken');
			axios.get(`/api/verify`)
				.then(res => {
					setIsAuth(true);
				})
				.catch(err => {
					setIsAuth(false);
					localStorage.removeItem('jwtToken');
				}
			);
		} else {
			setIsAuth(false);
		}
	}, []);


	return (
		<Router>
			<Container maxWidth="lg">
				<AppBar position="static">
					<Toolbar>
						{ isAuth && <Link to="/home" className={classes.button}>
							<Button color="inherit">Home</Button>
						</Link>}
						{ !isAuth && <Link to="/" className={classes.button}>
							<Button color="inherit">Log In</Button>
						</Link>}
						{ !isAuth && <Link to="/signup" className={classes.button}>
							<Button color="inherit" aria-label="signup">Sign up</Button>
						</Link>}
						{ isAuth && <Button color="inherit" onClick={logout}>Log out</Button>}
					</Toolbar>
				</AppBar>
				<div className={classes.main}>
					<Switch>
						<Route path="/home" exact>
							{ isAuth ? (() => <Home />) : (<Redirect to="/" />) }
						</Route>
						<Route exact path="/signup">
							{ isAuth ? (<Redirect to="/" />) : (<Signup register={register} message={signupMessage} />) }
						</Route>
						<Route path="/" exact>
							{ isAuth ? (<Redirect to="/home" />) : (<Login login={login} message={loginMessage} />) }
						</Route>
					</Switch>
				</div>
			</Container>
		</Router>	
  	);
}
  

export default App;
