import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
	Typography,
	Paper,
	Grid,
	TextField,
	Button
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';


/* styling */
const useStyles = makeStyles((theme) => ({
	root: {
	  '& > *': {
		marginTop: theme.spacing(4)
	  },
	},
	paper: {
		padding: theme.spacing(4)
	},
    sendBtn: {
        textAlign: 'center'
    },
	alert: {
		marginTop: theme.spacing(2)
	}
}));

type Props = {
	login: Function,
	message: string
};

function Login(props:Props) {
	const classes = useStyles();


	/* form fields */
	const [email, setEmail] = React.useState('');
	const [password, setPassword] = React.useState('');


	/* validation */
	const [validation, setValidation] = React.useState(false);
	const [validEmail, setValidEmail] = React.useState(false);
	const [emailValidationMsg, setEmailValidationMsg] = React.useState('');
	const [validPassword, setValidPassword] = React.useState(false);


    /*
        login when the validation succeeds
    */
	useEffect(() => {
		if( validEmail && validPassword ){
			props.login(email, password);
		}
	});


    /* validation logic */
	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		setValidation(true);
		setValidPassword(password.length > 0);

		if ( email.length === 0 ){
			setValidEmail(false);
			setEmailValidationMsg('Field can not be empty!');
		} else if( ! /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email) ){
			/*
				Simple regex for email format validation. I am aware that some of the RFC 5322 email addresses 
				can not be validated with this, but for the purpose of this exercise it should be OK.
			*/
			setValidEmail(false);
			setEmailValidationMsg('Field format not correct!');
		} else {
			setValidEmail(true);
		}
	}


	const handleChange = (value: string, field: string): void => {
		field === 'email' && setEmail(value);
		field === 'password' && setPassword(value);

		setValidation(false);
		setValidEmail(false);
		setValidPassword(false);
	}

	return (
		<Grid container spacing={3} justify="center">
			<Grid item xs={4}>
				<Paper className={classes.paper}>
					<Typography variant="h4" align="center">Log In</Typography>
					<form className={classes.root} autoComplete="off" onSubmit={handleSubmit}>
						<div>
							<TextField 
								error={!validEmail && validation}
								fullWidth
								id="login-email" 
								label="Email" 
								value={email}
								helperText={!validEmail && validation && emailValidationMsg}
								onChange={(e) => handleChange(e.target.value, 'email')}
							/>
						</div>
						<div>
							<TextField
								error={!validPassword && validation}
								fullWidth
								id="login-password"
								label="Password"
								type="password"
								value={password}
								helperText={!validPassword && validation && 'Field can not be empty!'}
								onChange={(e) => handleChange(e.target.value, 'password')}
							/>
						</div>
						<div className={classes.sendBtn}>
							<Button variant="contained" color="primary" type="submit" aria-label="send">
								Log In
							</Button>
						</div>
					</form>
					{ props.message !== '' && 
						<Alert 
							variant="outlined" 
							severity="error" 
							className={classes.alert}
						>
								{props.message}
						</Alert>
					}
				</Paper>
			</Grid>
		</Grid>
	);
}
 
export default Login;