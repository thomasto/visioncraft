import { 
	Typography,
} from '@material-ui/core';
import { useEffect, useState } from 'react';
import axios from 'axios';


function Home() {
    const [name, setName] = useState('');
    useEffect(() => {
		axios.get(`/api/name`)
			.then(res => {
				setName(res.data)
			});
	}, []);

    return (
        <Typography variant="h4" align="center">You are logged in as {name}!</Typography>
    );
}

export default Home;