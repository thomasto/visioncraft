import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
	Typography,
	Paper,
	Grid,
	TextField,
	Button
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';


const useStyles = makeStyles((theme) => ({
	root: {
	  '& > *': {
		marginTop: theme.spacing(4)
	  },
	},
	paper: {
		padding: theme.spacing(4)
	},
	centerBtn: {
		textAlign: 'center'
	},
	alert: {
		marginTop: theme.spacing(2)
	}
}));

type Props = {
	register: Function,
	message: string
};

  
function Signup(props:Props) {
	const classes = useStyles();


	//Fields
	const [firstname, setFirstname] = React.useState('');
	const [lastname, setLastname] = React.useState('');
	const [email, setEmail] = React.useState('');
	const [password, setPassword] = React.useState('');


	//Validation
	const [validation, setValidation] = React.useState(false);
	const [validEmail, setValidEmail] = React.useState(false);
	const [emailValidationMsg, setEmailValidationMsg] = React.useState('');
	const [validPassword, setValidPassword] = React.useState(false);
	const [validFirstname, setValidFirstname] = React.useState(false);
	const [validLastname, setValidLastname] = React.useState(false);


    /*
        login when the sign up succeeds
    */
	useEffect(() => {
		if( validation && validEmail && validPassword && validFirstname && validLastname ){
			props.register(email, password, firstname, lastname);
		}
	});

	const handleSubmit = (e: any) => {
		e.preventDefault();

		setValidation(true);
		setValidPassword(password.length > 0);
		setValidFirstname(firstname.length > 0);
		setValidLastname(lastname.length > 0);

		if ( email.length === 0 ){
			setValidEmail(false);
			setEmailValidationMsg('Field can not be empty!');
		} else if( ! /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email) ){
			setValidEmail(false);
			setEmailValidationMsg('Field format not correct!');
		} else {
			setValidEmail(true);
		}
	}

	const handleChange = (value: string, field: string): void => {
		field === 'email' && setEmail(value);
		field === 'password' && setPassword(value);
		field === 'firstname' && setFirstname(value);
		field === 'lastname' && setLastname(value);

		setValidation(false);
		setValidEmail(false);
		setValidPassword(false);
		setValidFirstname(false);
		setValidLastname(false);
	}

	return (
	  	<div>
			<Grid container spacing={3} justify="center">
        		<Grid item xs={4}>
					<Paper className={classes.paper}>
						<Typography variant="h4" align="center">Sign up</Typography>
						<form className={classes.root} autoComplete="off" onSubmit={handleSubmit}>
							<div>
								<TextField 
									error={!validFirstname && validation}
									fullWidth
									id="first-name" 
									label="First name" 
									value={firstname}
									helperText={!validFirstname && validation && 'Field can not be empty!'}
									onChange={(e) => handleChange(e.target.value, 'firstname')}
								/>
							</div>
							<div>
								<TextField 
									error={!validLastname && validation}
									fullWidth
									id="last-name" 
									label="Last name" 
									value={lastname}
									helperText={!validLastname && validation && 'Field can not be empty!'}
									onChange={(e) => handleChange(e.target.value, 'lastname')}
								/>
							</div>
							<div>
								<TextField 
									error={!validEmail && validation}
									fullWidth
									id="email" 
									label="Email" 
									value={email}
									helperText={!validEmail && validation && emailValidationMsg}
									onChange={(e) => handleChange(e.target.value, 'email')}
								/>
							</div>
							<div>
								<TextField
									error={!validPassword && validation}
									fullWidth
									id="password"
									label="Password"
									type="password"
									value={password}
									helperText={!validPassword && validation && 'Field can not be empty!'}
									onChange={(e) => handleChange(e.target.value, 'password')}
								/>
							</div>
							<div className={classes.centerBtn}>
								<Button variant="contained" color="primary" type="submit" aria-label="send">
									Sign up
								</Button>
							</div>
						</form>
						{ props.message !== '' && 
							<Alert 
								variant="outlined" 
								severity="error" 
								className={classes.alert}
							>
									{props.message}
							</Alert>
						}
					</Paper>
				</Grid>
			</Grid>
	  	</div>
	);
}
 
export default Signup;